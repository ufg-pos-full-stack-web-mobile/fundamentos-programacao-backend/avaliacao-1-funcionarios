package br.ufg.pos.fswm.fpb.avaliacao1funcionarios.dao;

import br.ufg.pos.fswm.fpb.avaliacao1funcionarios.model.FuncionarioDTO;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class FuncionarioRepositorioImpl implements FuncionarioRepositorio {

    private static final JdbcUtil jdbcUtil = JdbcUtil.getInstanceOf();

    @Override
    public FuncionarioDTO buscarPorCpf(String cpf) {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        FuncionarioDTO fun = null;

        try {
            conn = jdbcUtil.getConnection();

            final String sql = "SELECT f.nome, f.email, f.nascimento, f.telefone FROM funcionario f WHERE f.cpf = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, cpf);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                final String nome = rs.getString("nome");
                final String email = rs.getString("email");
                final String telefone = rs.getString("telefone");

                fun = new FuncionarioDTO();

                fun.setNome(nome);
                fun.setCpf(cpf);
                fun.setEmail(email);
                fun.setTelefone(telefone);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcUtil.close(conn, rs, pstmt);
        }

        return fun;
    }

    @Override
    public List<FuncionarioDTO> recuperarTodos() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        List<FuncionarioDTO> funcionarios = new ArrayList<>();

        try {
            conn = jdbcUtil.getConnection();

            final String sql = "SELECT f.nome, f.cpf, f.email, f.nascimento, f.telefone FROM funcionario f";
            pstmt = conn.prepareStatement(sql);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                final String nome = rs.getString("nome");
                final String email = rs.getString("email");
                final String cpf = rs.getString("cpf");
                final String telefone = rs.getString("telefone");

                final FuncionarioDTO fun = new FuncionarioDTO();

                fun.setNome(nome);
                fun.setEmail(email);
                fun.setCpf(cpf);
                fun.setTelefone(telefone);

                funcionarios.add(fun);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcUtil.close(conn, rs, pstmt);
        }

        return funcionarios;
    }

    @Override
    public void salvar(FuncionarioDTO funcionarioDTO) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = jdbcUtil.getConnection();

            final String sql = "INSERT INTO funcionario (nome, cpf, email, telefone) VALUES (?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, funcionarioDTO.getNome());
            pstmt.setString(2, funcionarioDTO.getCpf());
            pstmt.setString(3, funcionarioDTO.getEmail());
            pstmt.setString(4, funcionarioDTO.getTelefone());

            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcUtil.close(conn, pstmt);
        }

    }

    @Override
    public void deletarPorCpf(String cpf) {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            conn = jdbcUtil.getConnection();

            final String sql = "DELETE FROM funcionario WHERE cpf = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, cpf);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcUtil.close(conn, pstmt);
        }

    }
}
