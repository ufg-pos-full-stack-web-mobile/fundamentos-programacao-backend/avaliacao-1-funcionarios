package br.ufg.pos.fswm.fpb.avaliacao1funcionarios.dao;

import br.ufg.pos.fswm.fpb.avaliacao1funcionarios.model.FuncionarioDTO;

import java.util.List;

public interface FuncionarioRepositorio {

    FuncionarioDTO buscarPorCpf(String cpf);

    List<FuncionarioDTO> recuperarTodos();

    void salvar(FuncionarioDTO funcionarioDTO);

    void deletarPorCpf(String cpf);

}
