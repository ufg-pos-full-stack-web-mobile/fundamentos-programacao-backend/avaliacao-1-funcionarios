package br.ufg.pos.fswm.fpb.avaliacao1funcionarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Avaliacao1FuncionariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(Avaliacao1FuncionariosApplication.class, args);
	}
}
