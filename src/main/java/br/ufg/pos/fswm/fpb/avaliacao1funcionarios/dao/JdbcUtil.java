package br.ufg.pos.fswm.fpb.avaliacao1funcionarios.dao;

import org.springframework.beans.factory.annotation.Value;

import java.sql.*;

class JdbcUtil {

    private static JdbcUtil instance;

    @Value("${spring.datasource.url}")
    private String URL = "jdbc:postgresql://localhost:5432/db-funcionario";

    @Value("${spring.datasource.username}")
    private String USER = "postgres";

    @Value("${spring.datasource.password}")
    private String PASS = "495tDttwhn";

    private JdbcUtil() {
        // faz nada
    }

    static JdbcUtil getInstanceOf() {
        if (instance == null) {
            instance = new JdbcUtil();
        }
        return instance;
    }

    Connection getConnection() throws SQLException {
        return DriverManager
                .getConnection(URL, USER, PASS);
    }

    public void close(Connection conn) {
        close(conn, null, null);
    }

    public void close(Connection conn, Statement stmt) {
        close(conn, null, stmt);
    }

    public void close(Connection conn, ResultSet rs) {
        close(conn, rs, null);
    }

    public void close(Connection conn, ResultSet rs, Statement stmt) {
        try {
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
            if(stmt != null) {
                stmt.close();
            }
            if(rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            // Ai não tem o que fazer
        }
    }
}
