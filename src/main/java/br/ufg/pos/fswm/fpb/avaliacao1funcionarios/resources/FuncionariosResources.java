package br.ufg.pos.fswm.fpb.avaliacao1funcionarios.resources;

import br.ufg.pos.fswm.fpb.avaliacao1funcionarios.dao.FuncionarioRepositorio;
import br.ufg.pos.fswm.fpb.avaliacao1funcionarios.model.FuncionarioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/funcionarios")
public class FuncionariosResources {

    @Autowired
    private FuncionarioRepositorio repositorio;

    @GetMapping("/{cpf}")
    public FuncionarioDTO buscarPorCpf(@PathVariable("cpf") String cpf) {
        return repositorio.buscarPorCpf(cpf);
    }

    @PutMapping
    public void salvarNovo(@RequestBody FuncionarioDTO funcionario) {
        repositorio.salvar(funcionario);
    }

    @GetMapping
    public List<FuncionarioDTO> buscarTodos() {
        return repositorio.recuperarTodos();
    }

    @DeleteMapping("/{cpf}")
    public void deletar(@PathVariable("cpf") String cpf) {
        repositorio.deletarPorCpf(cpf);
    }
}
